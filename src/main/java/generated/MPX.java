//
// Questo file � stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andr� persa durante la ricompilazione dello schema di origine. 
// Generato il: 2018.10.26 alle 10:17:25 AM CEST 
//


package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/**
 * <p>Classe Java per anonymous complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Header">
 *           &lt;complexType>
 *             &lt;simpleContent>
 *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
 *                 &lt;attribute name="ZCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                 &lt;attribute name="Username" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                 &lt;attribute name="Password" type="{http://www.w3.org/2001/XMLSchema}string" />
 *               &lt;/extension>
 *             &lt;/simpleContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Set" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Pdf">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="Envelope" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="Data">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="AddressLine1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="AddressLine2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="AddressLine3" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="CAP" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                                                 &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="LocalCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="Country" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                               &lt;/sequence>
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                     &lt;/sequence>
 *                                     &lt;attribute name="PageStart" type="{http://www.w3.org/2001/XMLSchema}byte" />
 *                                     &lt;attribute name="PageEnd" type="{http://www.w3.org/2001/XMLSchema}byte" />
 *                                     &lt;attribute name="WorkProcessID" type="{http://www.w3.org/2001/XMLSchema}int" />
 *                                     &lt;attribute name="CustomerEnvelopeID" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                     &lt;attribute name="EUserZCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                     &lt;attribute name="DataNotifica" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                     &lt;attribute name="DataInserimentoAgenda" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                     &lt;attribute name="DataScadenza" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="PdfByteCode64" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                           &lt;attribute name="NumPages" type="{http://www.w3.org/2001/XMLSchema}short" />
 *                           &lt;attribute name="Type" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="CustomerPdfID" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *                 &lt;attribute name="Test" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                 &lt;attribute name="CustomerSetID" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                 &lt;attribute name="RagioneSocialeMittente" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                 &lt;attribute name="IndirizzoMittente" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                 &lt;attribute name="CapMittente" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                 &lt;attribute name="CittaMittente" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                 &lt;attribute name="ProvinciaMittente" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                 &lt;attribute name="NazioneMittente" type="{http://www.w3.org/2001/XMLSchema}string" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "header",
    "set"
})
@XmlRootElement(name = "MPX")
public class MPX { 

    @XmlElement(name = "Header", required = true)
    protected MPX.Header header;
    @XmlElement(name = "Set")
    protected List<MPX.Set> set;

    /**
     * Recupera il valore della propriet� header.
     * 
     * @return
     *     possible object is
     *     {@link MPX.Header }
     *     
     */
    public MPX.Header getHeader() {
        return header;
    }

    /**
     * Imposta il valore della propriet� header.
     * 
     * @param value
     *     allowed object is
     *     {@link MPX.Header }
     *     
     */
    public void setHeader(MPX.Header value) {
        this.header = value;
    }

    /**
     * Gets the value of the set property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the set property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSet().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MPX.Set }
     * 
     * 
     */
    public List<MPX.Set> getSet() {
        if (set == null) {
            set = new ArrayList<MPX.Set>();
        }
        return this.set;
    }


    /**
     * <p>Classe Java per anonymous complex type.
     * 
     * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;simpleContent>
     *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
     *       &lt;attribute name="ZCode" type="{http://www.w3.org/2001/XMLSchema}string" />
     *       &lt;attribute name="Username" type="{http://www.w3.org/2001/XMLSchema}string" />
     *       &lt;attribute name="Password" type="{http://www.w3.org/2001/XMLSchema}string" />
     *     &lt;/extension>
     *   &lt;/simpleContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "value"
    })
    public static class Header {
    	 public Header() {
    		 super();
    	 }
        public Header(String zCode, String username, String password) {
			super();
			this.zCode = zCode;
			this.username = username;
			this.password = password;
		}

		@XmlValue
        protected String value;
        @XmlAttribute(name = "ZCode")
        protected String zCode;
        @XmlAttribute(name = "Username")
        protected String username;
        @XmlAttribute(name = "Password")
        protected String password;

        /**
         * Recupera il valore della propriet� value.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getValue() {
            return value;
        }

        /**
         * Imposta il valore della propriet� value.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setValue(String value) {
            this.value = value;
        }

        /**
         * Recupera il valore della propriet� zCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getZCode() {
            return zCode;
        }

        /**
         * Imposta il valore della propriet� zCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setZCode(String value) {
            this.zCode = value;
        }

        /**
         * Recupera il valore della propriet� username.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUsername() {
            return username;
        }

        /**
         * Imposta il valore della propriet� username.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUsername(String value) {
            this.username = value;
        }

        /**
         * Recupera il valore della propriet� password.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPassword() {
            return password;
        }

        /**
         * Imposta il valore della propriet� password.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPassword(String value) {
            this.password = value;
        }

    }


    /**
     * <p>Classe Java per anonymous complex type.
     * 
     * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Pdf">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="Envelope" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="Data">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="AddressLine1" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="AddressLine2" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="AddressLine3" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="CAP" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                                       &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="LocalCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="Country" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                     &lt;/sequence>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                           &lt;/sequence>
     *                           &lt;attribute name="PageStart" type="{http://www.w3.org/2001/XMLSchema}byte" />
     *                           &lt;attribute name="PageEnd" type="{http://www.w3.org/2001/XMLSchema}byte" />
     *                           &lt;attribute name="WorkProcessID" type="{http://www.w3.org/2001/XMLSchema}int" />
     *                           &lt;attribute name="CustomerEnvelopeID" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                           &lt;attribute name="EUserZCode" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                           &lt;attribute name="DataNotifica" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                           &lt;attribute name="DataInserimentoAgenda" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                           &lt;attribute name="DataScadenza" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="PdfByteCode64" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *                 &lt;attribute name="NumPages" type="{http://www.w3.org/2001/XMLSchema}short" />
     *                 &lt;attribute name="Type" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="CustomerPdfID" type="{http://www.w3.org/2001/XMLSchema}string" />
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *       &lt;attribute name="Test" type="{http://www.w3.org/2001/XMLSchema}string" />
     *       &lt;attribute name="CustomerSetID" type="{http://www.w3.org/2001/XMLSchema}string" />
     *       &lt;attribute name="RagioneSocialeMittente" type="{http://www.w3.org/2001/XMLSchema}string" />
     *       &lt;attribute name="IndirizzoMittente" type="{http://www.w3.org/2001/XMLSchema}string" />
     *       &lt;attribute name="CapMittente" type="{http://www.w3.org/2001/XMLSchema}string" />
     *       &lt;attribute name="CittaMittente" type="{http://www.w3.org/2001/XMLSchema}string" />
     *       &lt;attribute name="ProvinciaMittente" type="{http://www.w3.org/2001/XMLSchema}string" />
     *       &lt;attribute name="NazioneMittente" type="{http://www.w3.org/2001/XMLSchema}string" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "pdf"
    })
    public static class Set {
    	public Set() {
    		super();
    	}
        public Set(String test, String customerSetID, String ragioneSocialeMittente, String indirizzoMittente,
				String capMittente, String cittaMittente, String provinciaMittente, String nazioneMittente) {
			super();
			this.test = test;
			this.customerSetID = customerSetID;
			this.ragioneSocialeMittente = ragioneSocialeMittente;
			this.indirizzoMittente = indirizzoMittente;
			this.capMittente = capMittente;
			this.cittaMittente = cittaMittente;
			this.provinciaMittente = provinciaMittente;
			this.nazioneMittente = nazioneMittente;
		}


		@XmlElement(name = "Pdf", required = true)
        protected MPX.Set.Pdf pdf;
        @XmlAttribute(name = "Test")
        protected String test;
        @XmlAttribute(name = "CustomerSetID")
        protected String customerSetID;
        @XmlAttribute(name = "RagioneSocialeMittente")
        protected String ragioneSocialeMittente;
        @XmlAttribute(name = "IndirizzoMittente")
        protected String indirizzoMittente;
        @XmlAttribute(name = "CapMittente")
        protected String capMittente;
        @XmlAttribute(name = "CittaMittente")
        protected String cittaMittente;
        @XmlAttribute(name = "ProvinciaMittente")
        protected String provinciaMittente;
        @XmlAttribute(name = "NazioneMittente")
        protected String nazioneMittente;

        /**
         * Recupera il valore della propriet� pdf.
         * 
         * @return
         *     possible object is
         *     {@link MPX.Set.Pdf }
         *     
         */
        public MPX.Set.Pdf getPdf() {
            return pdf;
        }

        /**
         * Imposta il valore della propriet� pdf.
         * 
         * @param value
         *     allowed object is
         *     {@link MPX.Set.Pdf }
         *     
         */
        public void setPdf(MPX.Set.Pdf value) {
            this.pdf = value;
        }

        /**
         * Recupera il valore della propriet� test.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTest() {
            return test;
        }

        /**
         * Imposta il valore della propriet� test.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTest(String value) {
            this.test = value;
        }

        /**
         * Recupera il valore della propriet� customerSetID.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCustomerSetID() {
            return customerSetID;
        }

        /**
         * Imposta il valore della propriet� customerSetID.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCustomerSetID(String value) {
            this.customerSetID = value;
        }

        /**
         * Recupera il valore della propriet� ragioneSocialeMittente.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRagioneSocialeMittente() {
            return ragioneSocialeMittente;
        }

        /**
         * Imposta il valore della propriet� ragioneSocialeMittente.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRagioneSocialeMittente(String value) {
            this.ragioneSocialeMittente = value;
        }

        /**
         * Recupera il valore della propriet� indirizzoMittente.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIndirizzoMittente() {
            return indirizzoMittente;
        }

        /**
         * Imposta il valore della propriet� indirizzoMittente.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIndirizzoMittente(String value) {
            this.indirizzoMittente = value;
        }

        /**
         * Recupera il valore della propriet� capMittente.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCapMittente() {
            return capMittente;
        }

        /**
         * Imposta il valore della propriet� capMittente.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCapMittente(String value) {
            this.capMittente = value;
        }

        /**
         * Recupera il valore della propriet� cittaMittente.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCittaMittente() {
            return cittaMittente;
        }

        /**
         * Imposta il valore della propriet� cittaMittente.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCittaMittente(String value) {
            this.cittaMittente = value;
        }

        /**
         * Recupera il valore della propriet� provinciaMittente.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getProvinciaMittente() {
            return provinciaMittente;
        }

        /**
         * Imposta il valore della propriet� provinciaMittente.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setProvinciaMittente(String value) {
            this.provinciaMittente = value;
        }

        /**
         * Recupera il valore della propriet� nazioneMittente.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNazioneMittente() {
            return nazioneMittente;
        }

        /**
         * Imposta il valore della propriet� nazioneMittente.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNazioneMittente(String value) {
            this.nazioneMittente = value;
        }


        /**
         * <p>Classe Java per anonymous complex type.
         * 
         * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="Envelope" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="Data">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="AddressLine1" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="AddressLine2" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="AddressLine3" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="CAP" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *                             &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="LocalCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="Country" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                 &lt;/sequence>
         *                 &lt;attribute name="PageStart" type="{http://www.w3.org/2001/XMLSchema}byte" />
         *                 &lt;attribute name="PageEnd" type="{http://www.w3.org/2001/XMLSchema}byte" />
         *                 &lt;attribute name="WorkProcessID" type="{http://www.w3.org/2001/XMLSchema}int" />
         *                 &lt;attribute name="CustomerEnvelopeID" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                 &lt;attribute name="EUserZCode" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                 &lt;attribute name="DataNotifica" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                 &lt;attribute name="DataInserimentoAgenda" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                 &lt;attribute name="DataScadenza" type="{http://www.w3.org/2001/XMLSchema}string" />
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="PdfByteCode64" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *       &lt;attribute name="NumPages" type="{http://www.w3.org/2001/XMLSchema}short" />
         *       &lt;attribute name="Type" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="CustomerPdfID" type="{http://www.w3.org/2001/XMLSchema}string" />
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "envelope",
            "pdfByteCode64"
        })
        public static class Pdf {

            @XmlElement(name = "Envelope")
            protected List<MPX.Set.Pdf.Envelope> envelope;
            @XmlElement(name = "PdfByteCode64", required = true)
            protected String pdfByteCode64;
            @XmlAttribute(name = "NumPages")
            protected String numPages;
            @XmlAttribute(name = "Type")
            protected String type;
            @XmlAttribute(name = "CustomerPdfID")
            protected String customerPdfID;
            
            public Pdf() {
            	super();
            }

            public Pdf(String numPages, String type, String customerPdfID) {
				super();
				this.numPages = numPages;
				this.type = type;
				this.customerPdfID = customerPdfID;
			}

			/**
             * Gets the value of the envelope property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the envelope property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getEnvelope().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link MPX.Set.Pdf.Envelope }
             * 
             * 
             */
            public List<MPX.Set.Pdf.Envelope> getEnvelope() {
                if (envelope == null) {
                    envelope = new ArrayList<MPX.Set.Pdf.Envelope>();
                }
                return this.envelope;
            }

            /**
             * Recupera il valore della propriet� pdfByteCode64.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPdfByteCode64() {
                return pdfByteCode64;
            }

            /**
             * Imposta il valore della propriet� pdfByteCode64.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPdfByteCode64(String value) {
                this.pdfByteCode64 = value;
            }

            /**
             * Recupera il valore della propriet� numPages.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNumPages() {
                return numPages;
            }

            /**
             * Imposta il valore della propriet� numPages.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNumPages(String value) {
                this.numPages = value;
            }

            /**
             * Recupera il valore della propriet� type.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getType() {
                return type;
            }

            /**
             * Imposta il valore della propriet� type.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setType(String value) {
                this.type = value;
            }

            /**
             * Recupera il valore della propriet� customerPdfID.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCustomerPdfID() {
                return customerPdfID;
            }

            /**
             * Imposta il valore della propriet� customerPdfID.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCustomerPdfID(String value) {
                this.customerPdfID = value;
            }


            /**
             * <p>Classe Java per anonymous complex type.
             * 
             * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="Data">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="AddressLine1" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="AddressLine2" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="AddressLine3" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="CAP" type="{http://www.w3.org/2001/XMLSchema}int"/>
             *                   &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="LocalCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="Country" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *       &lt;/sequence>
             *       &lt;attribute name="PageStart" type="{http://www.w3.org/2001/XMLSchema}byte" />
             *       &lt;attribute name="PageEnd" type="{http://www.w3.org/2001/XMLSchema}byte" />
             *       &lt;attribute name="WorkProcessID" type="{http://www.w3.org/2001/XMLSchema}int" />
             *       &lt;attribute name="CustomerEnvelopeID" type="{http://www.w3.org/2001/XMLSchema}string" />
             *       &lt;attribute name="EUserZCode" type="{http://www.w3.org/2001/XMLSchema}string" />
             *       &lt;attribute name="DataNotifica" type="{http://www.w3.org/2001/XMLSchema}string" />
             *       &lt;attribute name="DataInserimentoAgenda" type="{http://www.w3.org/2001/XMLSchema}string" />
             *       &lt;attribute name="DataScadenza" type="{http://www.w3.org/2001/XMLSchema}string" />
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "data"
            })
            public static class Envelope {

                @XmlElement(name = "Data", required = true)
                protected MPX.Set.Pdf.Envelope.Data data;
                @XmlAttribute(name = "PageStart")
                protected Integer pageStart;
                @XmlAttribute(name = "PageEnd")
                protected Integer pageEnd;
                @XmlAttribute(name = "WorkProcessID")
                protected String workProcessID;
                @XmlAttribute(name = "CustomerEnvelopeID")
                protected String customerEnvelopeID;
                @XmlAttribute(name = "EUserZCode")
                protected String eUserZCode;
                @XmlAttribute(name = "DataNotifica")
                protected String dataNotifica;
                @XmlAttribute(name = "DataInserimentoAgenda")
                protected String dataInserimentoAgenda;
                @XmlAttribute(name = "DataScadenza")
                protected String dataScadenza;
                

                public Envelope() {
                	super();
                }
                
                
                public Envelope(int pageStart, int pageEnd, String workProcessID, String customerEnvelopeID,
						String eUserZCode, String dataNotifica, String dataInserimentoAgenda, String dataScadenza) {
					super();
					this.pageStart = pageStart;
					this.pageEnd = pageEnd;
					this.workProcessID = workProcessID;
					this.customerEnvelopeID = customerEnvelopeID;
					this.eUserZCode = eUserZCode;
					this.dataNotifica = dataNotifica;
					this.dataInserimentoAgenda = dataInserimentoAgenda;
					this.dataScadenza = dataScadenza;
				}

				/**
                 * Recupera il valore della propriet� data.
                 * 
                 * @return
                 *     possible object is
                 *     {@link MPX.Set.Pdf.Envelope.Data }
                 *     
                 */
                public MPX.Set.Pdf.Envelope.Data getData() {
                    return data;
                }

                /**
                 * Imposta il valore della propriet� data.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link MPX.Set.Pdf.Envelope.Data }
                 *     
                 */
                public void setData(MPX.Set.Pdf.Envelope.Data value) {
                    this.data = value;
                }

                /**
                 * Recupera il valore della propriet� pageStart.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Byte }
                 *     
                 */
                public Integer getPageStart() {
                    return pageStart;
                }

                /**
                 * Imposta il valore della propriet� pageStart.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Byte }
                 *     
                 */
                public void setPageStart(Integer value) {
                    this.pageStart = value;
                }

                /**
                 * Recupera il valore della propriet� pageEnd.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Byte }
                 *     
                 */
                public Integer getPageEnd() {
                    return pageEnd;
                }

                /**
                 * Imposta il valore della propriet� pageEnd.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Byte }
                 *     
                 */
                public void setPageEnd(Integer value) {
                    this.pageEnd = value;
                }

                /**
                 * Recupera il valore della propriet� workProcessID.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Integer }
                 *     
                 */
                public String getWorkProcessID() {
                    return workProcessID;
                }

                /**
                 * Imposta il valore della propriet� workProcessID.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Integer }
                 *     
                 */
                public void setWorkProcessID(String value) {
                    this.workProcessID = value;
                }

                /**
                 * Recupera il valore della propriet� customerEnvelopeID.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCustomerEnvelopeID() {
                    return customerEnvelopeID;
                }

                /**
                 * Imposta il valore della propriet� customerEnvelopeID.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCustomerEnvelopeID(String value) {
                    this.customerEnvelopeID = value;
                }

                /**
                 * Recupera il valore della propriet� eUserZCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getEUserZCode() {
                    return eUserZCode;
                }

                /**
                 * Imposta il valore della propriet� eUserZCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setEUserZCode(String value) {
                    this.eUserZCode = value;
                }

                /**
                 * Recupera il valore della propriet� dataNotifica.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDataNotifica() {
                    return dataNotifica;
                }

                /**
                 * Imposta il valore della propriet� dataNotifica.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDataNotifica(String value) {
                    this.dataNotifica = value;
                }

                /**
                 * Recupera il valore della propriet� dataInserimentoAgenda.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDataInserimentoAgenda() {
                    return dataInserimentoAgenda;
                }

                /**
                 * Imposta il valore della propriet� dataInserimentoAgenda.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDataInserimentoAgenda(String value) {
                    this.dataInserimentoAgenda = value;
                }

                /**
                 * Recupera il valore della propriet� dataScadenza.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDataScadenza() {
                    return dataScadenza;
                }

                /**
                 * Imposta il valore della propriet� dataScadenza.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDataScadenza(String value) {
                    this.dataScadenza = value;
                }


                /**
                 * <p>Classe Java per anonymous complex type.
                 * 
                 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="AddressLine1" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="AddressLine2" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="AddressLine3" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="CAP" type="{http://www.w3.org/2001/XMLSchema}int"/>
                 *         &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="LocalCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="Country" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "addressLine1",
                    "addressLine2",
                    "addressLine3",
                    "cap",
                    "city",
                    "localCode",
                    "country"
                })
                public static class Data {

                    @XmlElement(name = "AddressLine1", required = true)
                    protected String addressLine1;
                    @XmlElement(name = "AddressLine2", required = true)
                    protected String addressLine2;
                    @XmlElement(name = "AddressLine3", required = true)
                    protected String addressLine3;
                    @XmlElement(name = "CAP")
                    protected String cap;
                    @XmlElement(name = "City", required = true)
                    protected String city;
                    @XmlElement(name = "LocalCode", required = true)
                    protected String localCode;
                    @XmlElement(name = "Country", required = true)
                    protected String country;

                    /**
                     * Recupera il valore della propriet� addressLine1.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getAddressLine1() {
                        return addressLine1;
                    }

                    /**
                     * Imposta il valore della propriet� addressLine1.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setAddressLine1(String value) {
                        this.addressLine1 = value;
                    }

                    /**
                     * Recupera il valore della propriet� addressLine2.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getAddressLine2() {
                        return addressLine2;
                    }

                    /**
                     * Imposta il valore della propriet� addressLine2.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setAddressLine2(String value) {
                        this.addressLine2 = value;
                    }

                    /**
                     * Recupera il valore della propriet� addressLine3.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getAddressLine3() {
                        return addressLine3;
                    }

                    /**
                     * Imposta il valore della propriet� addressLine3.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setAddressLine3(String value) {
                        this.addressLine3 = value;
                    }

                    /**
                     * Recupera il valore della propriet� cap.
                     * 
                     */
                    public String getCAP() {
                        return cap;
                    }

                    /**
                     * Imposta il valore della propriet� cap.
                     * 
                     */
                    public void setCAP(String value) {
                        this.cap = value;
                    }

                    /**
                     * Recupera il valore della propriet� city.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getCity() {
                        return city;
                    }

                    /**
                     * Imposta il valore della propriet� city.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setCity(String value) {
                        this.city = value;
                    }

                    /**
                     * Recupera il valore della propriet� localCode.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getLocalCode() {
                        return localCode;
                    }

                    /**
                     * Imposta il valore della propriet� localCode.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setLocalCode(String value) {
                        this.localCode = value;
                    }

                    /**
                     * Recupera il valore della propriet� country.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getCountry() {
                        return country;
                    }

                    /**
                     * Imposta il valore della propriet� country.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setCountry(String value) {
                        this.country = value;
                    }

                }

            }

        }

    }

}
