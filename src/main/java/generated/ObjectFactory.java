//
// Questo file � stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andr� persa durante la ricompilazione dello schema di origine. 
// Generato il: 2018.10.26 alle 10:17:25 AM CEST 
//


package generated;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the generated package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: generated
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link MPX }
     * 
     */
    public MPX createMPX() {
        return new MPX();
    }

    /**
     * Create an instance of {@link MPX.Set }
     * 
     */
    public MPX.Set createMPXSet() {
        return new MPX.Set();
    }

    /**
     * Create an instance of {@link MPX.Set.Pdf }
     * 
     */
    public MPX.Set.Pdf createMPXSetPdf() {
        return new MPX.Set.Pdf();
    }

    /**
     * Create an instance of {@link MPX.Set.Pdf.Envelope }
     * 
     */
    public MPX.Set.Pdf.Envelope createMPXSetPdfEnvelope() {
        return new MPX.Set.Pdf.Envelope();
    }

    /**
     * Create an instance of {@link MPX.Header }
     * 
     */
    public MPX.Header createMPXHeader() {
        return new MPX.Header();
    }

    /**
     * Create an instance of {@link MPX.Set.Pdf.Envelope.Data }
     * 
     */
    public MPX.Set.Pdf.Envelope.Data createMPXSetPdfEnvelopeData() {
        return new MPX.Set.Pdf.Envelope.Data();
    }

}
