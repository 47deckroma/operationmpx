//
// Questo file � stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andr� persa durante la ricompilazione dello schema di origine. 
// Generato il: 2018.11.16 alle 02:31:34 PM CET 
//


package adobe.parser.bookmarks;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/**
 * <p>Classe Java per anonymous complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Bookmark" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Title" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Action">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="GoTo">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="Dest">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="XYZ">
 *                                                   &lt;complexType>
 *                                                     &lt;simpleContent>
 *                                                       &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
 *                                                         &lt;attribute name="Left" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                         &lt;attribute name="PageNum" type="{http://www.w3.org/2001/XMLSchema}integer" />
 *                                                         &lt;attribute name="Top" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                         &lt;attribute name="Zoom" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                       &lt;/extension>
 *                                                     &lt;/simpleContent>
 *                                                   &lt;/complexType>
 *                                                 &lt;/element>
 *                                               &lt;/sequence>
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *       &lt;attribute name="version" type="{http://www.w3.org/2001/XMLSchema}float" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "bookmark"
})
@XmlRootElement(name = "Bookmarks")
public class Bookmarks {

    @XmlElement(name = "Bookmark")
    protected List<Bookmarks.Bookmark> bookmark;
    @XmlAttribute(name = "version")
    protected Float version;

    /**
     * Gets the value of the bookmark property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the bookmark property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBookmark().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Bookmarks.Bookmark }
     * 
     * 
     */
    public List<Bookmarks.Bookmark> getBookmark() {
        if (bookmark == null) {
            bookmark = new ArrayList<Bookmarks.Bookmark>();
        }
        return this.bookmark;
    }

    /**
     * Recupera il valore della propriet� version.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    public Float getVersion() {
        return version;
    }

    /**
     * Imposta il valore della propriet� version.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setVersion(Float value) {
        this.version = value;
    }


    /**
     * <p>Classe Java per anonymous complex type.
     * 
     * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Title" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Action">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="GoTo">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="Dest">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="XYZ">
     *                                         &lt;complexType>
     *                                           &lt;simpleContent>
     *                                             &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
     *                                               &lt;attribute name="Left" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                               &lt;attribute name="PageNum" type="{http://www.w3.org/2001/XMLSchema}integer" />
     *                                               &lt;attribute name="Top" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                               &lt;attribute name="Zoom" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                             &lt;/extension>
     *                                           &lt;/simpleContent>
     *                                         &lt;/complexType>
     *                                       &lt;/element>
     *                                     &lt;/sequence>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "title",
        "action"
    })
    public static class Bookmark {

        @XmlElement(name = "Title", required = true)
        protected String title;
        @XmlElement(name = "Action", required = true)
        protected Bookmarks.Bookmark.Action action;

        /**
         * Recupera il valore della propriet� title.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTitle() {
            return title;
        }

        /**
         * Imposta il valore della propriet� title.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTitle(String value) {
            this.title = value;
        }

        /**
         * Recupera il valore della propriet� action.
         * 
         * @return
         *     possible object is
         *     {@link Bookmarks.Bookmark.Action }
         *     
         */
        public Bookmarks.Bookmark.Action getAction() {
            return action;
        }

        /**
         * Imposta il valore della propriet� action.
         * 
         * @param value
         *     allowed object is
         *     {@link Bookmarks.Bookmark.Action }
         *     
         */
        public void setAction(Bookmarks.Bookmark.Action value) {
            this.action = value;
        }


        /**
         * <p>Classe Java per anonymous complex type.
         * 
         * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="GoTo">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="Dest">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="XYZ">
         *                               &lt;complexType>
         *                                 &lt;simpleContent>
         *                                   &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
         *                                     &lt;attribute name="Left" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                     &lt;attribute name="PageNum" type="{http://www.w3.org/2001/XMLSchema}integer" />
         *                                     &lt;attribute name="Top" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                     &lt;attribute name="Zoom" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                   &lt;/extension>
         *                                 &lt;/simpleContent>
         *                               &lt;/complexType>
         *                             &lt;/element>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "goTo"
        })
        public static class Action {

            @XmlElement(name = "GoTo", required = true)
            protected Bookmarks.Bookmark.Action.GoTo goTo;

            /**
             * Recupera il valore della propriet� goTo.
             * 
             * @return
             *     possible object is
             *     {@link Bookmarks.Bookmark.Action.GoTo }
             *     
             */
            public Bookmarks.Bookmark.Action.GoTo getGoTo() {
                return goTo;
            }

            /**
             * Imposta il valore della propriet� goTo.
             * 
             * @param value
             *     allowed object is
             *     {@link Bookmarks.Bookmark.Action.GoTo }
             *     
             */
            public void setGoTo(Bookmarks.Bookmark.Action.GoTo value) {
                this.goTo = value;
            }


            /**
             * <p>Classe Java per anonymous complex type.
             * 
             * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="Dest">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="XYZ">
             *                     &lt;complexType>
             *                       &lt;simpleContent>
             *                         &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
             *                           &lt;attribute name="Left" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                           &lt;attribute name="PageNum" type="{http://www.w3.org/2001/XMLSchema}integer" />
             *                           &lt;attribute name="Top" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                           &lt;attribute name="Zoom" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                         &lt;/extension>
             *                       &lt;/simpleContent>
             *                     &lt;/complexType>
             *                   &lt;/element>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "dest"
            })
            public static class GoTo {

                @XmlElement(name = "Dest", required = true)
                protected Bookmarks.Bookmark.Action.GoTo.Dest dest;

                /**
                 * Recupera il valore della propriet� dest.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Bookmarks.Bookmark.Action.GoTo.Dest }
                 *     
                 */
                public Bookmarks.Bookmark.Action.GoTo.Dest getDest() {
                    return dest;
                }

                /**
                 * Imposta il valore della propriet� dest.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Bookmarks.Bookmark.Action.GoTo.Dest }
                 *     
                 */
                public void setDest(Bookmarks.Bookmark.Action.GoTo.Dest value) {
                    this.dest = value;
                }


                /**
                 * <p>Classe Java per anonymous complex type.
                 * 
                 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="XYZ">
                 *           &lt;complexType>
                 *             &lt;simpleContent>
                 *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
                 *                 &lt;attribute name="Left" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *                 &lt;attribute name="PageNum" type="{http://www.w3.org/2001/XMLSchema}integer" />
                 *                 &lt;attribute name="Top" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *                 &lt;attribute name="Zoom" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *               &lt;/extension>
                 *             &lt;/simpleContent>
                 *           &lt;/complexType>
                 *         &lt;/element>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "xyz"
                })
                public static class Dest {

                    @XmlElement(name = "XYZ", required = true)
                    protected Bookmarks.Bookmark.Action.GoTo.Dest.XYZ xyz;

                    /**
                     * Recupera il valore della propriet� xyz.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Bookmarks.Bookmark.Action.GoTo.Dest.XYZ }
                     *     
                     */
                    public Bookmarks.Bookmark.Action.GoTo.Dest.XYZ getXYZ() {
                        return xyz;
                    }

                    /**
                     * Imposta il valore della propriet� xyz.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Bookmarks.Bookmark.Action.GoTo.Dest.XYZ }
                     *     
                     */
                    public void setXYZ(Bookmarks.Bookmark.Action.GoTo.Dest.XYZ value) {
                        this.xyz = value;
                    }


                    /**
                     * <p>Classe Java per anonymous complex type.
                     * 
                     * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
                     * 
                     * <pre>
                     * &lt;complexType>
                     *   &lt;simpleContent>
                     *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
                     *       &lt;attribute name="Left" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *       &lt;attribute name="PageNum" type="{http://www.w3.org/2001/XMLSchema}integer" />
                     *       &lt;attribute name="Top" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *       &lt;attribute name="Zoom" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *     &lt;/extension>
                     *   &lt;/simpleContent>
                     * &lt;/complexType>
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "value"
                    })
                    public static class XYZ {

                        @XmlValue
                        protected String value;
                        @XmlAttribute(name = "Left")
                        protected String left;
                        @XmlAttribute(name = "PageNum")
                        protected Integer pageNum;
                        @XmlAttribute(name = "Top")
                        protected String top;
                        @XmlAttribute(name = "Zoom")
                        protected String zoom;

                        /**
                         * Recupera il valore della propriet� value.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getValue() {
                            return value;
                        }

                        /**
                         * Imposta il valore della propriet� value.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setValue(String value) {
                            this.value = value;
                        }

                        /**
                         * Recupera il valore della propriet� left.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getLeft() {
                            return left;
                        }

                        /**
                         * Imposta il valore della propriet� left.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setLeft(String value) {
                            this.left = value;
                        }

                        /**
                         * Recupera il valore della propriet� pageNum.
                         * 
                         * @return
                         *     possible object is
                         *     {@link BigInteger }
                         *     
                         */
                        public Integer getPageNum() {
                            return pageNum;
                        }

                        /**
                         * Imposta il valore della propriet� pageNum.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link BigInteger }
                         *     
                         */
                        public void setPageNum(Integer value) {
                            this.pageNum = value;
                        }

                        /**
                         * Recupera il valore della propriet� top.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getTop() {
                            return top;
                        }

                        /**
                         * Imposta il valore della propriet� top.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setTop(String value) {
                            this.top = value;
                        }

                        /**
                         * Recupera il valore della propriet� zoom.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getZoom() {
                            return zoom;
                        }

                        /**
                         * Imposta il valore della propriet� zoom.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setZoom(String value) {
                            this.zoom = value;
                        }

                    }

                }

            }

        }

    }

}
