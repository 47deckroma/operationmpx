//
// Questo file � stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andr� persa durante la ricompilazione dello schema di origine. 
// Generato il: 2018.11.16 alle 02:31:34 PM CET 
//


package adobe.parser.bookmarks;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the adobe.parser.bookmarks package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: adobe.parser.bookmarks
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Bookmarks }
     * 
     */
    public Bookmarks createBookmarks() {
        return new Bookmarks();
    }

    /**
     * Create an instance of {@link Bookmarks.Bookmark }
     * 
     */
    public Bookmarks.Bookmark createBookmarksBookmark() {
        return new Bookmarks.Bookmark();
    }

    /**
     * Create an instance of {@link Bookmarks.Bookmark.Action }
     * 
     */
    public Bookmarks.Bookmark.Action createBookmarksBookmarkAction() {
        return new Bookmarks.Bookmark.Action();
    }

    /**
     * Create an instance of {@link Bookmarks.Bookmark.Action.GoTo }
     * 
     */
    public Bookmarks.Bookmark.Action.GoTo createBookmarksBookmarkActionGoTo() {
        return new Bookmarks.Bookmark.Action.GoTo();
    }

    /**
     * Create an instance of {@link Bookmarks.Bookmark.Action.GoTo.Dest }
     * 
     */
    public Bookmarks.Bookmark.Action.GoTo.Dest createBookmarksBookmarkActionGoToDest() {
        return new Bookmarks.Bookmark.Action.GoTo.Dest();
    }

    /**
     * Create an instance of {@link Bookmarks.Bookmark.Action.GoTo.Dest.XYZ }
     * 
     */
    public Bookmarks.Bookmark.Action.GoTo.Dest.XYZ createBookmarksBookmarkActionGoToDestXYZ() {
        return new Bookmarks.Bookmark.Action.GoTo.Dest.XYZ();
    }

}
