package it.adobe.deck47.hdi.OperationMPX.beanInstance;

import it.adobe.deck47.beans.EsitoResponseMPX;

public class EsitoMPX_Utility   {
	
	/**
	 * 
	 */

	public static String toString(EsitoResponseMPX esitoMPXD) {
		return "ResponseMPX [Type=" +   ", returnCode=" + esitoMPXD.getReturnCodeInt()+ ", returnString=" + esitoMPXD.getReturnCodeString()
				+ "]";
	}
	
	public static void setReturnRESPONSECODE(RESPONSECODE responseCode, EsitoResponseMPX esitoMPXD) {
		esitoMPXD.setReturnCodeInt(responseCode.getErrCode());
		esitoMPXD.setReturnCodeString(responseCode.getErrString());
	}

}
