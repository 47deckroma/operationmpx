package it.adobe.deck47.hdi.OperationMPX.create;

import java.util.ArrayList;

import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;

import generated.MPX.Set.Pdf.Envelope;
import it.adobe.deck47.beans.EsitoResponseMPX;
import it.adobe.deck47.beans.ItemQuery;
import it.mpx.queries.MPXQuery.Queries.QueryCheckStatus.Set;

public class ParserResponse {
	/*es:
	 * tag=Header
	 * itemCode=GlobalCode
	 * visibility :package
	 * */
	public static EsitoResponseMPX parserReturnMessage(org.w3c.dom.Document  doc,String tag,String itemCode) throws Exception{
		EsitoResponseMPX responseValue = new EsitoResponseMPX();
		NodeList nodes = doc.getElementsByTagName(tag);
	 for (int i = 0; i < nodes.getLength(); i++) {
	      Element element = (Element) nodes.item(i);
	      NamedNodeMap nodeMap = element.getAttributes() ;
	      int code = Integer.parseInt( nodeMap.getNamedItem(itemCode).getNodeValue());
	      responseValue.setReturnCodeInt(code);
	      if(code!=0) {
	    	  	if(nodeMap.getNamedItem("Message")!=null && nodeMap.getNamedItem("Message").getNodeValue()!=null && nodeMap.getNamedItem("Message").getNodeValue().length()>0)
	    	  		responseValue.setReturnCodeString(  nodeMap.getNamedItem("Message").getNodeValue() );
	    	  	else
	    	  		responseValue.setReturnCodeString("Errore riscontrato nel TAG <"+tag+">" );
	    	  	return responseValue;
			}
	 }
	 return responseValue;
	}
	
	public static boolean containTag(org.w3c.dom.Document  doc,String tag ) throws Exception{
		NodeList nodes = doc.getElementsByTagName(tag);
	 if( nodes.getLength()>0) {
	       return true;
	 }
	 return false;
	}
	
	public static String getNodeValue(NamedNodeMap nodeMap,String value) {
		String val = "";
		try {
		  val =  nodeMap.getNamedItem(value).getNodeValue();
		}catch(Exception e) {val = "";}
		if(val!=null)return val;
		return "";
	}
	
	public static ArrayList<ItemQuery> getListParserMessage(org.w3c.dom.Document  doc,String tag, Class className) throws Exception{
		EsitoResponseMPX responseValue = new EsitoResponseMPX();
		NodeList nodes = doc.getElementsByTagName(tag);
		ArrayList<ItemQuery> lista = new ArrayList<ItemQuery>();
	 for (int i = 0; i < nodes.getLength(); i++) {
	      Element element = (Element) nodes.item(i);
	      NamedNodeMap nodeMap = element.getAttributes() ;
	      if(className.getName().equalsIgnoreCase(Set.class.getName())) {
	    	  ItemQuery item = new ItemQuery();
	    	  item.setCustomerSetID(getNodeValue(nodeMap,"CustomerSetID"));
	    	  item.setMPXSetID(getNodeValue(nodeMap,"MPXSetID"));
	    	  item.setCode(getNodeValue(nodeMap,"Code"));
	    	  item.setNotifyDate(getNodeValue(nodeMap,"NotifyDate"));
//	    	  String status = getNodeValue(nodeMap,"Status");
//	    	  if(status!=null && !status.isEmpty()) {
//	    		  status="1"+status; 
//	    	  }
	    	  item.setStatus(getNodeValue(nodeMap,"Status"));
	    	  item.setUploadDate(getNodeValue(nodeMap,"UploadDate"));
	    	  item.setType("Set");
	    	lista.add(item);
	      }else if(className.getName().equalsIgnoreCase(Envelope.class.getName()) ) {
	    	  ItemQuery item = new ItemQuery();
	    	  item.setCustomerEnvelopeID(getNodeValue(nodeMap,"CustomerEnvelopeID"));
	    	  item.setCustomerSetID(getNodeValue(nodeMap,"CustomerSetID"));
	    	  item.setMPXSetID(getNodeValue(nodeMap,"MPXSetID"));
	    	  item.setCodRaccomandata( getNodeValue(nodeMap,"CodRaccomandata"));
	    	  item.setMailDate( getNodeValue(nodeMap,"MailDate"));
	    	  item.setType("Envelope");
	    	lista.add(item);
	      }
	 }
	 return lista;
	}

}
