package it.adobe.deck47.hdi.OperationMPX;

import java.io.StringReader;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.util.function.BiConsumer;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import adobe.parser.bookmarks.Bookmarks;
import generated.MPX.Set.Pdf.Envelope;
import hdi.lotto.Lotto;
import hdi.lotto.Lotto.Documento;
import hdi.lotto.Lotto.Documento.Testata.Destinatari.Soggetto;
import hdi.lotto.Lotto.Servizio.Mittente;
import hdi.lotto.OperazioniXMLLotto;
import it.adobe.deck47.ReadConfigFile;
import it.adobe.deck47.beans.EsitoResponseMPX;
import it.adobe.deck47.beans.ItemQuery;
import it.adobe.deck47.hdi.QueryElement;
import it.adobe.deck47.hdi.OperationMPX.beanInstance.EsitoMPX_Utility;
import it.adobe.deck47.hdi.OperationMPX.beanInstance.MPXazioni;
import it.adobe.deck47.hdi.OperationMPX.beanInstance.RESPONSECODE;
import it.adobe.deck47.hdi.OperationMPX.create.CreateRequest;
import it.adobe.deck47.hdi.OperationMPX.create.CreateRequest.TEST;
import it.adobe.deck47.hdi.OperationMPX.create.ParserFlusso;
import it.adobe.deck47.hdi.OperationMPX.send.SendPostHTTPS;
import it.adobe.deck47.hdi.exception.CustomException;
import it.deck47.db.DBOperationsHDI;
import it.mpx.queries.MPXQuery.Queries.GetRegLetterNote;
import it.mpx.queries.MPXQuery.Queries.QueryCheckStatus;
import it.mpx.queries.MPXQuery.Queries.QueryCheckStatus.Set;

public class OperationMPXOperations {
    private static final Logger log = Logger.getLogger( OperationMPXOperations.class.getName() );
	
    
    protected enum CODICI_POSTALIZZATI{POSTEL};
    //private static String pathPropertyFile=null;
    
    public static void main(String[] args) throws Exception {
    	
    	
    	java.util.HashMap<String,String> hashMapvalori = new java.util.HashMap<String,String>();
    	//for (int i =0 ;i<10;i++) addQuery(lista,QueryElement.CustomerSetID,"value"+i);
    	//addQuery(hashMapvalori,QueryElement.CustomerSetID,"SI0000055486");//QCS
    	//EsitoResponseMPX result = OperationMPXOperations.createMPXQueryQCS( hashMapvalori);
    	
		
		addQuery(hashMapvalori,QueryElement.CustomerSetID,"SI0000054379");//RLN 
		EsitoResponseMPX result = OperationMPXOperations.createMPXQueryRLN(hashMapvalori);
		
		System.out.println("QUERY:"+EsitoMPX_Utility.toString(result)+ " " + result.getMPXString());
		 
		OperationMPXOperations op = new OperationMPXOperations();
		//result = op.sendToPostel(result.getMPXString(), "https://upload.printel.com/mpx/xml/upload.asp", false,"UPLOAD");
		result = op.sendToPostel(result.getMPXString(), "http://postpdx.postel.it/MpxQuery.ashx", false,"QUERY");
		//System.out.println("Esito:"+EsitoMPX_Utility.toString(result)+ " " + result.getMPXString());
		
		int i=1;
		for (ItemQuery item : result.getListaItemQuery()) {
			System.out.println(i+") "+item.toString());
			i++;
		}
		DBOperationsHDI db = new DBOperationsHDI();
		//db.updateQCS(result.getListaItemQuery());

    }
    
	// http://localhost:8080/RESTfulExample/json/product/post
	public EsitoResponseMPX sendToPostel( String xmlInput ,String URLMPX , boolean stampaMPX ,String type ) throws Exception   {
		EsitoResponseMPX response = new EsitoResponseMPX();
		if(xmlInput==null) {
			EsitoMPX_Utility.setReturnRESPONSECODE(RESPONSECODE.ERR_XML,response);
			return response;
		}
		 //SEND
		  SendPostHTTPS send = new SendPostHTTPS();
		  if(stampaMPX)   log.info("Send MPX to:"+URLMPX+"\n"+xmlInput);
		  
		  response  =  send.Send(URLMPX,xmlInput,type);
		  if(response.getReturnCodeInt()==0) {
			  EsitoMPX_Utility.setReturnRESPONSECODE(RESPONSECODE.OK,response);
		  }else {
		  }
		  return response;
	}
	
	
	public static java.util.HashMap<String,String> addQuery(java.util.HashMap<String,String> valori,QueryElement key ,String value) {
		if( valori==null ) valori = new java.util.HashMap<String,String>();
		boolean insert=false;
		String keyName= key.name();
		while(insert==false) {
			if(valori.containsKey(keyName)) {
				//System.out.println(keyName);
				keyName = keyName+ new Random().nextInt(100);
			}else {
				valori.put(keyName, value);
				insert = true;
			}
		}
		return valori;
	}
	
	
	 public static EsitoResponseMPX createMPXQueryRLN(  java.util.HashMap<String,String> valori) throws Exception {
		 EsitoResponseMPX response = new EsitoResponseMPX();
		 List<GetRegLetterNote> listaLettere  = new ArrayList<GetRegLetterNote>();
		 ReadConfigFile p = new ReadConfigFile();
		  Properties props = p.getProperty(false);
		 BiConsumer<String, String> biConsumer = (x, y) -> {
			 GetRegLetterNote lettera = new GetRegLetterNote();
			 if(x.startsWith("CustomerSetID"))
				 lettera.setCustomerSetID(y);
			 if(x.startsWith("MPXSetID"))
				 lettera.setMPXSetID(y);
			 if(x.startsWith("CustomerSetID_MPXSetID")) {
				 lettera.setCustomerSetID(y.split("_")[0]);
				 lettera.setMPXSetID(y.split("_")[1]);
			 }
			 if(x.startsWith("MPXSetID_CustomerSetID")) {
				 lettera.setCustomerSetID(y.split("_")[1]);
				 lettera.setMPXSetID(y.split("_")[0]);
			 }
			 listaLettere.add(lettera);
			 if(lettera.getCustomerSetID()==null && lettera.getMPXSetID()==null) {
					EsitoMPX_Utility.setReturnRESPONSECODE(RESPONSECODE.ERR_QUERIES_NOLETTERDATA,response);
					 
			 }
		 };
		valori.forEach(biConsumer);
		 if(response.getReturnCodeInt()!=-100) {
			 return response;
		 }
		 if(listaLettere.size()<1) {
				EsitoMPX_Utility.setReturnRESPONSECODE(RESPONSECODE.ERR_QUERIES_NOLETTER,response);
				return response;
			}
		 String mpx = CreateRequest.createMPXQuery(props.getProperty("ZCode"),props.getProperty("Username"),props.getProperty("Password"),listaLettere,null);
		 response.setMPXString(mpx);
		 EsitoMPX_Utility.setReturnRESPONSECODE(RESPONSECODE.OK,response);
		 return response;
	 }
	 
	 
	 public static EsitoResponseMPX createMPXQueryQCS(    java.util.HashMap<String,String> valori) throws Exception {
		 EsitoResponseMPX response = new EsitoResponseMPX();
		 List<Set> listaSet  = new ArrayList<Set>();
		 ReadConfigFile p = new ReadConfigFile();
		  Properties props = p.getProperty(false);
		 BiConsumer<String, String> biConsumer = (x, y) -> {
			 QueryCheckStatus qcs = new QueryCheckStatus();
			 
			 Set setVar = new Set();
			 if(x.startsWith("CustomerSetID"))
				 setVar.setCustomerSetID(y);
			 if(x.startsWith("MPXSetID"))
				 setVar.setMPXSetID(y);
			 if(x.startsWith("CustomerSetID_MPXSetID")) {
				 setVar.setCustomerSetID(y.split("_")[0]);
				 setVar.setMPXSetID(y.split("_")[1]);
			 }
			 if(x.startsWith("MPXSetID_CustomerSetID")) {
				 setVar.setCustomerSetID(y.split("_")[1]);
				 setVar.setMPXSetID(y.split("_")[0]);
			 }
			 listaSet.add(setVar);
			 if(setVar.getCustomerSetID()==null && setVar.getMPXSetID()==null) {
					EsitoMPX_Utility.setReturnRESPONSECODE(RESPONSECODE.ERR_QUERIES_NOLETTERDATA,response);
					 
			 }
		 };
		 
		valori.forEach(biConsumer);
		 if(response.getReturnCodeInt()!=-100) {
			 return response;
		 }
		 if(listaSet.size()<1) {
				EsitoMPX_Utility.setReturnRESPONSECODE(RESPONSECODE.ERR_QUERIES_NOLETTER,response);
				return response;
			}
		 String mpx = CreateRequest.createMPXQuery(props.getProperty("ZCode"),props.getProperty("Username"),props.getProperty("Password"),null,listaSet);
		 response.setMPXString(mpx);
		 EsitoMPX_Utility.setReturnRESPONSECODE(RESPONSECODE.OK,response);
		 return response;
	 }
	 
//	 private String getCustomerSetID(Lotto f) {
//		 return  f.getTemplate()+"_"+f.getPeriodoRiferimento()+"_"+f.getIdentificativoLotto();
//	 }
//	
//	 public String getCustomerEnvelopID( String xmlInput  ) throws Exception   {
//		 	if(xmlInput==null) {
//				throw new CustomException(RESPONSECODE.ERR_XML.name());
//			}
//			Lotto f;
//			try {
//				f = ParserFlusso.unmarshallFlusso(xmlInput);
//			} catch (Exception e) {
//				e.printStackTrace();
//				throw new CustomException(RESPONSECODE.ERR_UNMARSH.name());
//			}
//			return  getCustomerSetID(f);
//	 }
	// http://localhost:8080/RESTfulExample/json/product/post
	 
	 public EsitoResponseMPX createMPX( String xmlInput ,String totalPDFPage ,String pdfEncode64 ,
			   String xmlBookmarks , MPXazioni mpxAzioniParam) throws Exception   {
		 ReadConfigFile p = new ReadConfigFile();
		 return createMPX(xmlInput, totalPDFPage, pdfEncode64, xmlBookmarks, mpxAzioniParam, p);
	 }
	 
	 public EsitoResponseMPX createMPX_PropPath( String xmlInput ,String totalPDFPage ,String pdfEncode64 ,
			   String xmlBookmarks , MPXazioni mpxAzioniParam,String path) throws Exception   {
		 ReadConfigFile p = new ReadConfigFile();
		 p.setpath(path);
		 return createMPX(xmlInput, totalPDFPage, pdfEncode64, xmlBookmarks, mpxAzioniParam, p);
	 }
	 
		private EsitoResponseMPX createMPX( String xmlInput ,String totalPDFPage ,String pdfEncode64 ,
				   String xmlBookmarks , MPXazioni mpxAzioniParam,ReadConfigFile p ) throws Exception   {
			EsitoResponseMPX response = new EsitoResponseMPX();
			
			  Properties props = p.getProperty(true);
			try {
			if(xmlInput==null) {
				EsitoMPX_Utility.setReturnRESPONSECODE(RESPONSECODE.ERR_XML,response);
				throw new CustomException(RESPONSECODE.ERR_XML.name());
			}
			Lotto f;
			Bookmarks b;
			try {
				f = ParserFlusso.unmarshallFlusso(xmlInput);
			} catch (Exception e) {
				e.printStackTrace();
				EsitoMPX_Utility.setReturnRESPONSECODE(RESPONSECODE.ERR_UNMARSH,response);
				throw new CustomException(RESPONSECODE.ERR_UNMARSH.name());
			}
			try {
				b = ParserFlusso.unmarshallBkms(xmlBookmarks);
			} catch (Exception e) {
				e.printStackTrace();
				EsitoMPX_Utility.setReturnRESPONSECODE(RESPONSECODE.ERR_UNMARSHBOOKMARKS,response);
				throw new CustomException(RESPONSECODE.ERR_UNMARSHBOOKMARKS.name());
			}
			
			//Create ENVELOPS
//			ReadConfigFile props = new ReadConfigFile();
//			String ZCode= props.getPropertyValue(false, "ZCode");
//			String Username = props.getPropertyValue(false, "Username");
//			String Password = props.getPropertyValue(false, "Password");
//			String test = props.getPropertyValue(false, "TEST");
//			String tipoAmbiente = props.getPropertyValue(false, "TipoAmbiente");
//			String URLMPX = props.getPropertyValue(false, "URLMPX");
			
					String workProcessID = f.getServizio().getTipoInvio();
					String dataNotifica ="";
					String dataInserimentoAgenda ="";
					String dataScadenza ="";
					String eUserZCode =f.getServizio().getCentroDiCosto();
					
			 ArrayList<Envelope> envelops = new ArrayList<>();
			 int bookmarksCurrent = 0;
			 int paginaFine = 0;
			for (Documento doc :  f.getDocumento()) {
				String CODICE ="VUOTO";
				if(doc.getTestata()!=null && doc.getTestata().getServizio()!=null  && doc.getTestata().getServizio().getCodice()!=null )
				   CODICE = doc.getTestata().getServizio().getCodice();
				//log.info(CODICE);
				boolean postalizza = false;
				
				if(mpxAzioniParam == MPXazioni.ARCHIVIAZIONE || mpxAzioniParam == MPXazioni.ARCHIVIAZIONEONLINE ) {//Se devo archiviare utilizo tutto l'xml
					postalizza = true;
				}else {
					for (CODICI_POSTALIZZATI postalizzati : CODICI_POSTALIZZATI.values()) {
						if(CODICE.equalsIgnoreCase(postalizzati.name())) {
							postalizza = true;
						}
					}
				}
				
				if(!postalizza) {
					log.warning("Documento servizio.codice '"+CODICE+"' non postalizzato: " + doc.getContratto().getNumeroPolizza());
					continue;
				}
				int i=0;
				for (Soggetto sogetto : doc.getTestata().getDestinatari().getSoggetto()) {
					i++;
					log.info("Sogetto :" + i +" "+sogetto.getNominativo()  );
					String CustomerEnvelopeID = OperazioniXMLLotto.getCustomerEnvelopeID(doc, props.getProperty("TipoAmbiente"));
					
					int paginaInizio = b.getBookmark().get(bookmarksCurrent).getAction().getGoTo().getDest().getXYZ().getPageNum()+1;
					
					if(b.getBookmark().size()> (bookmarksCurrent+1)) {
						// if(stampaMPX)   log.info("Bookmanrks:"+b.getBookmark().size()+">"+(bookmarksCurrent+1));
					   paginaFine = b.getBookmark().get(bookmarksCurrent+1).getAction().getGoTo().getDest().getXYZ().getPageNum();
					}else {
						//if(stampaMPX)   log.info("Bookmanrks Fine:"+b.getBookmark().size()+"!>"+(bookmarksCurrent+1));
						paginaFine = Integer.parseInt(totalPDFPage);
					}
					 Envelope envel = new Envelope(paginaInizio, paginaFine,workProcessID, CustomerEnvelopeID, eUserZCode,dataNotifica, dataInserimentoAgenda,dataScadenza);
					 String Add3 = sogetto.getRecapito().getToponimo()+" "+ sogetto.getRecapito().getIndirizzo()+" "+ sogetto.getRecapito().getNumeroCivico();
					 CreateRequest.setEnvData(envel,sogetto.getNominativo(),"",Add3,sogetto.getRecapito().getCap(),sogetto.getRecapito().getComune(),sogetto.getRecapito().getSiglaProvincia(),sogetto.getRecapito().getNazione().getCodice());
		    		 envelops.add(envel);
		    		 bookmarksCurrent++;
				}
			}
			
			if(paginaFine != Integer.parseInt(totalPDFPage)){
				String er = "Attenzione, le pagine da postalizzare e il numero di pagine nel PDF sono differenti ; Pagine totali PDF:"+totalPDFPage+ " pagine XML :" +paginaFine;
				log.severe(er);
				EsitoMPX_Utility.setReturnRESPONSECODE(RESPONSECODE.ERR_POSTALIZZATE,response);
				response.setReturnCodeString(er);
				throw new CustomException(RESPONSECODE.ERR_POSTALIZZATE.name());
			}
			//CREATE XML
			  String xml;
			try {
				String CustomerSetID = OperazioniXMLLotto.getCustomerSetID(f);
				response.setCustomerSetID(CustomerSetID);
				 String CustomerPDFID = CustomerSetID;//Al momento i due valori sono lo stesso 
				 Mittente mittente = f.getServizio().getMittente();
				 if(mittente!=null) {
				xml = CreateRequest.createXML(TEST.valueOf(props.getProperty("TEST")), pdfEncode64, props.getProperty("ZCode"),props.getProperty("Username"),props.getProperty("Password"), CustomerSetID , totalPDFPage,CustomerPDFID,
						f.getServizio().getMittente().getRagioneSociale(),
						f.getServizio().getMittente().getIndirizzo(),
						f.getServizio().getMittente().getCap(),
						f.getServizio().getMittente().getCitta(),
						f.getServizio().getMittente().getProvincia(),
						f.getServizio().getMittente().getNazione(),  
						envelops);
				 }else {
					 xml = CreateRequest.createXML(TEST.valueOf(props.getProperty("TEST")), pdfEncode64, props.getProperty("ZCode"),props.getProperty("Username"),props.getProperty("Password"), CustomerSetID , totalPDFPage,CustomerPDFID,
								"",
								"",
								"",
								"",
								"",
								"",  
								envelops);
				 }

			} catch (Exception e) {
				e.printStackTrace();
				EsitoMPX_Utility.setReturnRESPONSECODE(RESPONSECODE.ERR_MARSH,response);
				throw new CustomException(RESPONSECODE.ERR_MARSH.name());
			} 
			try {
				response.setMpxDocument( convertStringToXMLDocument(xml));
			}catch (Exception e) {
				e.printStackTrace();
				EsitoMPX_Utility.setReturnRESPONSECODE(RESPONSECODE.ERR_CREATEMXPDOCUMENT,response);
				throw new CustomException(RESPONSECODE.ERR_CREATEMXPDOCUMENT.name());
			}
			response.setMPXString(xml);
			EsitoMPX_Utility.setReturnRESPONSECODE(RESPONSECODE.OK,response);
			
			}catch(CustomException custExc) {
				log.severe("Errore inatteso nel Component.OperationMPX.createMPX");
				if(response.getReturnCodeInt()>=0) {
					EsitoMPX_Utility.setReturnRESPONSECODE(RESPONSECODE.ERR_GENERICO,response);
				}
			}
			return response;
		}
		
		
		
		
		
		
		
		
		public EsitoResponseMPX getPropertiesMPX( Document xmlInput  ) throws Exception   {
			EsitoResponseMPX response = new EsitoResponseMPX();
			try {
					if(xmlInput==null) {
						EsitoMPX_Utility.setReturnRESPONSECODE(RESPONSECODE.ERR_XML,response);
						throw new CustomException(RESPONSECODE.ERR_XML.name());
					}
					Lotto f;
					try {
						f = ParserFlusso.unmarshallFlusso(xmlInput);
					} catch (Exception e) {
						e.printStackTrace();
						EsitoMPX_Utility.setReturnRESPONSECODE(RESPONSECODE.ERR_UNMARSH,response);
						throw new CustomException(RESPONSECODE.ERR_UNMARSH.name());
					}
					response.setCodiceAgenzia ( f.getDocumento().get(0).getContratto().getPuntoVendita().getCodice());
					response.setCodiceCausale(f.getDocumento().get(0).getContratto().getCausale().getCodice());
					response.setCodiceDocumento ( f.getCodiceDocumento() );
					response.setIdMovimento(f.getDocumento().get(0).getContratto().getIdMovimento());
					response.setNumeroPolizza(f.getDocumento().get(0).getContratto().getNumeroPolizza());
					response.setTipologiaDocumento(f.getTipologiaDms());
					
					
					if(checkString(response.getCodiceAgenzia()) && checkString(response.getCodiceCausale()) && checkString(response.getCodiceDocumento())
							&& checkString(response.getIdMovimento()) &&
							checkString(response.getNumeroPolizza()) && checkString(response.getTipologiaDocumento())  ) {
						EsitoMPX_Utility.setReturnRESPONSECODE(RESPONSECODE.OK,response);
					}else {
						EsitoMPX_Utility.setReturnRESPONSECODE(RESPONSECODE.ERR_GENERICO,response);
					}
//					for (Documento doc :  f.getDocumento()) {
//					}
					
			}catch(Exception e) {
				e.printStackTrace();
				EsitoMPX_Utility.setReturnRESPONSECODE(RESPONSECODE.ERR_GENERICO,response);
			}
			
			return response;
		}
		
		private boolean checkString(String check) {
			if(check!=null) {
				if(!check.isEmpty()) {
					return true;
				}
			}
			return false;
		}
	
		public static String convertXmlToString(org.w3c.dom.Document xmlDocument)
		{
		    TransformerFactory tf = TransformerFactory.newInstance();
		    Transformer transformer;
		    try {
		        transformer = tf.newTransformer();
		        // Uncomment if you do not require XML declaration
		        // transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
		        //A character stream that collects its output in a string buffer,
		        //which can then be used to construct a string.
		        StringWriter writer = new StringWriter();
		        transformer.transform(new DOMSource(xmlDocument), new StreamResult(writer));
		        String xmlString = writer.getBuffer().toString();  
		        return xmlString;
		    }
		    catch (TransformerException e)
		    {
		        e.printStackTrace();
		    }
		    catch (Exception e)
		    {
		        e.printStackTrace();
		    }
			return null;
		}
		
		public static org.w3c.dom.Document convertStringToXMLDocument(String xmlString) throws Exception
	    {
	        //Parser that produces DOM object trees from XML content
	        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	        //API to obtain DOM Document instance
	        DocumentBuilder builder = null;
	        //Create DocumentBuilder with default configuration
	        builder = factory.newDocumentBuilder();
	        //Parse the content to Document object
	        return builder.parse(new InputSource(new StringReader(xmlString)));
	    }
		
	
	public org.w3c.dom.Document getXMLBase64(String xmlStr){
		byte[] xmlByte = xmlStr.getBytes(StandardCharsets.UTF_8);
		System.out.println(xmlStr);		
		byte[] decode= Base64.getDecoder().decode(xmlByte); 
		String s1 = Arrays.toString(decode);
		String s2 = new String(decode);

		System.out.println(s1);        // -> "[97, 98, 99]"
		System.out.println(s2); 
		/*
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();  
		DocumentBuilder builder;  
		org.w3c.dom.Document doc= null;
		try {
			builder = factory.newDocumentBuilder();  
			doc = builder.parse(new InputSource(decode.toString()));
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		return doc;
		 */
		return null;
	}
	
	
	

}
