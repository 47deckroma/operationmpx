package it.adobe.deck47.hdi.OperationMPX.beanInstance;

public enum RESPONSECODE {ERR_XML("XML input nullo o errato",-10),
	ERR_UNMARSH("XML non conforme a quanto atteso.Unmarshall in erroe",-11),
	ERR_MARSH("Creazione XML request in errore.Marshall in erroe",-12),
	ERR_SEND("Errore durante la chiamatqa al servizio",-13),
	ERR_UNMARSHBOOKMARKS("Errore durante la codifica del bookmarks",-14),
	ERR_POSTALIZZATE("Errore pagine PDF-bookmarks",-15),
	ERR_CREATEMXPDOCUMENT("Errore conversione XML string to Document",-16),
	ERR_QUERIES_NOLETTER("Queries : Richiesta vuota",-17),
	ERR_QUERIES_NOLETTERDATA("Queries : CustomerSetID e MPXSetID mancante",-18),
	ERR_GENERICO("GENERICO",-19),
	OK("OK",0);
	private String errString;
	private int errCode;
	
	
	private RESPONSECODE(String errString, int errCode) {
		this.setErrString(errString);
		this.setErrCode(errCode);
	}
	public String getErrString() {
		return errString;
	}
	public void setErrString(String errString) {
		this.errString = errString;
	}
	public int getErrCode() {
		return errCode;
	}
	public void setErrCode(int errCode) {
		this.errCode = errCode;
	}

}
