package it.adobe.deck47.hdi.OperationMPX.create;

import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.w3c.dom.Document;

import adobe.parser.bookmarks.Bookmarks;
import hdi.lotto.Lotto;

public class ParserFlusso {

	
	public static Lotto unmarshallFlusso(String xmlInput) throws Exception {
				Lotto f = (Lotto) unmarsharl(Lotto.class,xmlInput );
			//System.out.println(f);
			//System.out.println(f.getTestata().getTemplate());
			//System.out.println( f.getDocumenti().getDocumento().get(0).getTestata().getDestinatari().getSoggetto().get(0).getNominativo());
			return f;
	}
	
	public static Lotto unmarshallFlusso(Document xmlInput) throws Exception {
		Lotto f = (Lotto) unmarsharl(Lotto.class,xmlInput );
	//System.out.println(f);
	//System.out.println(f.getTestata().getTemplate());
	//System.out.println( f.getDocumenti().getDocumento().get(0).getTestata().getDestinatari().getSoggetto().get(0).getNominativo());
	return f;
}
	
	public static Bookmarks unmarshallBkms(String xmlInput) throws Exception {
		Bookmarks f = (Bookmarks) unmarsharl(Bookmarks.class,xmlInput );
	 return f;
}
	
	
	public static Object unmarsharl(Class jaxbObject ,String xmlinput ) throws Exception {
        JAXBContext jc = JAXBContext.newInstance(jaxbObject);
        Unmarshaller unmarshaller = jc.createUnmarshaller();
        StringReader sr = new StringReader(xmlinput);
        Object tests =  unmarshaller.unmarshal(sr);//cast (jaxbObject.getClass())
        //SystemOut
        //Marshaller marshaller = jc.createMarshaller();
        //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        //marshaller.marshal(tests, System.out);
        return tests;
    }
	
	public static Object unmarsharl(Class jaxbObject ,Document xmlinput ) throws Exception {
        JAXBContext jc = JAXBContext.newInstance(jaxbObject);
        Unmarshaller unmarshaller = jc.createUnmarshaller();
        Object tests =  unmarshaller.unmarshal(xmlinput);//cast (jaxbObject.getClass())
        //SystemOut
        //Marshaller marshaller = jc.createMarshaller();
        //marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        //marshaller.marshal(tests, System.out);
        return tests;
    }
}
