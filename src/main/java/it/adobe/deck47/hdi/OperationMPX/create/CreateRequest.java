package it.adobe.deck47.hdi.OperationMPX.create;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import generated.MPX;
import generated.MPX.Header;
import generated.MPX.Set;
import generated.MPX.Set.Pdf;
import generated.MPX.Set.Pdf.Envelope;
import generated.MPX.Set.Pdf.Envelope.Data;
import it.mpx.queries.MPXQuery;
import it.mpx.queries.MPXQuery.Queries;
import it.mpx.queries.MPXQuery.Queries.GetRegLetterNote;
import it.mpx.queries.MPXQuery.Queries.QueryCheckStatus;

public class CreateRequest {
	
	public enum TEST{ON,OFF};
	
	
	 public static void main( String [] args ) throws Exception {
		 //Envelope envel = new Envelope(1, 10,"workProcessID", "customerEnvelopeID", "eUserZCode", "02/10/2018 0.00.00", "05-11-2018", "05-11-2018");
		 //setEnvData(envel,"Add1","","Add3","00012","Guidonia","RM","IT");
		 //ArrayList<Envelope> envelops = new ArrayList<>();
		 //envelops.add(envel);
		 //String  workProcessID, String customerEnvelopeID, String eUserZCode, String dataNotifica, String dataInserimentoAgenda,String  dataScadenza,
		 //"workProcessID", "customerEnvelopeID", "eUserZCode", "02/10/2018 0.00.00", "05-11-2018", "05-11-2018",
//		 createXML(TEST.ON, "pdfEncode64", "zcode","username", "password", "customerSetID" ,
//				 1000,"customerPDFid",envelops
//				 );
		 //CreateRequest.createMPXQuery("ww","us","pw");
	 }
	 
	 public static String createMPXQuery(  String zcode, String username, String password , List<GetRegLetterNote> listaLettere, List<it.mpx.queries.MPXQuery.Queries.QueryCheckStatus.Set> listaSet) throws  Exception {
		 MPXQuery mpx = new MPXQuery();
         it.mpx.queries.MPXQuery.Header head = new it.mpx.queries.MPXQuery.Header(zcode,username,password);
         mpx.setHeader(head);
         Queries queries = new Queries();
         
         if(listaLettere!=null && listaLettere.size()>0) {
		         for (GetRegLetterNote lettera : listaLettere) {
			         queries.getGetRegLetterNote().add(lettera);
		         }
         } else if(listaSet!=null && listaSet.size()>0) {
		         queries.setQueryCheckStatus(new QueryCheckStatus());
		         for (it.mpx.queries.MPXQuery.Queries.QueryCheckStatus.Set set : listaSet) {
			         queries.getQueryCheckStatus().getSet().add(set);
		         }
         }else {
        	 throw new Exception("Nessun elemento presente nella lista delle query");
         }
         
		 mpx.setQueries(queries);
		 return marshallToString( mpx ,true);
     }
	 
	 public static MPX createHEADERXML(  String zcode, String username, String password ) throws  Exception {
         MPX mpx = new MPX();
         Header head = new Header(zcode,username,password);
         mpx.setHeader(head);
         return  mpx ;
     }
	
	 public static String createXML( TEST test ,String pdfEncode64, String zcode, String username, String password ,String customerSetID,
			 String PDFtotalPage, String CustomerPdfID,
			 String ragioneSocialeMittente, String indirizzoMittente, String capMittente, String cittaMittente, String provinciaMittente, String nazioneMittente
			 ,ArrayList<Envelope> envelops
			 ) throws  Exception {
         MPX mpx = createHEADERXML(zcode,username,password);
         Set set = new Set(test.name(),customerSetID,ragioneSocialeMittente, indirizzoMittente, capMittente, cittaMittente, provinciaMittente, nazioneMittente);
         mpx.getSet().add(set);
         Pdf pdf = new Pdf(PDFtotalPage,"online",CustomerPdfID);
		pdf.getEnvelope().addAll(envelops);
         set.setPdf(pdf);
		pdf.setPdfByteCode64(pdfEncode64);
         return marshallToString( mpx,false );
     }
	 
	 
	 public static void setEnvData(Envelope envel,String Add1,String Add2 ,String Add3, String CAP ,String City,String Prov,String Stato) {
		 Data data = new Data();
		 data.setAddressLine1(Add1);
		 data.setAddressLine2(Add2);
		 data.setAddressLine3(Add3);
		 data.setCAP(CAP);
		 data.setCity(City);
		 data.setCountry(Stato);
		 data.setLocalCode(Prov);
		 envel.setData(data);
 	 }
	
	public static void main2(String[] args) {
        DocumentBuilderFactory icFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder icBuilder;
        try {
            icBuilder = icFactory.newDocumentBuilder();
            Document doc = icBuilder.newDocument();
            //Element mainRootElement = doc.createElementNS("https://crunchify.com/CrunchifyCreateXMLDOM", "Companies");
            Element mainRootElement = doc.createElement("MPX");
            doc.appendChild(mainRootElement);
 
            // append child elements to root element
            //String [] array = {"","","",""};
            //ArrayList<String> attributes = (ArrayList<String>) Arrays.asList(array);// new ArrayList<String>( ["",""] ) ;
            Map <String,String > hm = new HashMap<String, String>();
            hm.put("att1", "valore1");
            mainRootElement.appendChild(getElementComplexType("Envelope",hm,doc,   "Paypal", "Payment", "1000"));
            mainRootElement.appendChild(getElementComplexType("Envelope",hm,doc,   "eBay", "Shopping", "2000"));
            mainRootElement.appendChild(getElementComplexType("Envelope",hm,doc,   "Google", "Search", "3000"));
 
            toStringDocument(doc);
 
            System.out.println("\nXML DOM Created Successfully..");
 
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
	
	
	private static void toStringDocument( Document doc ) throws TransformerFactoryConfigurationError, TransformerException {
		// output DOM XML to console 
        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "YES"); 
        DOMSource source = new DOMSource(doc);
        StreamResult console = new StreamResult(System.out);
        transformer.transform(source, console);
	}
	
	public static void marshallSysout( Object jaxbObject ) throws JAXBException {
        JAXBContext context = JAXBContext.newInstance(   jaxbObject.getClass() );
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,Boolean.TRUE);
        marshaller.marshal(jaxbObject, System.out);
    }
	
	public static String marshallToString( Object jaxbObject,boolean xmlDeclare ) throws JAXBException {
        JAXBContext context = JAXBContext.newInstance(   jaxbObject.getClass() );
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,Boolean.TRUE);
        if(!xmlDeclare)
        marshaller.setProperty("com.sun.xml.bind.xmlDeclaration", Boolean.FALSE);

        StringWriter sw = new StringWriter();
        marshaller.marshal(jaxbObject, sw);
        return sw.toString();
    }
	
	
	
	private static Node getElementComplexType(String elementName,Map<String,String> attributes,Document doc,   String name, String age, String role) {
        Element company = doc.createElement(elementName);
       for(String attributeKey : attributes.keySet()) {
    	   company.setAttribute(attributeKey, attributes.get(attributeKey));
       }
        company.appendChild(getCompanyElements(doc, company, "Name", name));
        company.appendChild(getCompanyElements(doc, company, "Type", age));
        company.appendChild(getCompanyElements(doc, company, "Employees", role));
        return company;
    }
 
    // utility method to create text node
    private static Node getCompanyElements(Document doc, Element element, String name, String value) {
        Element node = doc.createElement(name);
        node.appendChild(doc.createTextNode(value));
        return node;
    }

}
