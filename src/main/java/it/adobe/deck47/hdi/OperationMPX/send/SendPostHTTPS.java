package it.adobe.deck47.hdi.OperationMPX.send;

import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Logger;

import javax.net.ssl.HttpsURLConnection;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import generated.MPX.Set.Pdf.Envelope;
import it.adobe.deck47.beans.EsitoResponseMPX;
import it.adobe.deck47.beans.ItemQuery;
import it.adobe.deck47.hdi.App;
import it.adobe.deck47.hdi.OperationMPX.OperationMPXOperations;
import it.adobe.deck47.hdi.OperationMPX.create.ParserResponse;
import it.mpx.queries.MPXQuery.Queries.QueryCheckStatus.Set;


public class SendPostHTTPS {
    private static final Logger log = Logger.getLogger( SendPostHTTPS.class.getName() );

	public static void main(String[] args) throws FileNotFoundException, Exception {
		//String xmlInput = App.readFile( "assets\\XMLTest\\testMPXresponse.xml");
		//Document doc = OperationMPXOperations.convertStringToXMLDocument(xmlInput);
		//SendPostHTTPS send = new SendPostHTTPS();
		//EsitoResponseMPX res = send.parserReturnMessageMPX(doc);
		//System.out.println( res.getReturnCodeInt() + "" +res.getReturnCodeString());
	}
	
	private EsitoResponseMPX parserReturnMessageMPX(org.w3c.dom.Document  doc) throws Exception{
		EsitoResponseMPX responseValue = new EsitoResponseMPX();
		responseValue = ParserResponse.parserReturnMessage(doc,"Header","GlobalCode");
		if(responseValue.getReturnCodeInt()!=0)return responseValue;
		
		responseValue = ParserResponse.parserReturnMessage(doc,"Set","SetCode");
		if(responseValue.getReturnCodeInt()!=0)return responseValue;
		
		responseValue = ParserResponse.parserReturnMessage(doc,"Pdf","PdfCode");
		if(responseValue.getReturnCodeInt()!=0)return responseValue;
		
		responseValue = ParserResponse.parserReturnMessage(doc,"Envelope","EnvelopeCode");
		if(responseValue.getReturnCodeInt()!=0)return responseValue;
		
		 /*
		 NodeList Logs = doc.getElementsByTagName("Logs");
		 for (int i = 0; i < Logs.getLength(); i++) {
		      Node node =  Logs.item(i);
		      NodeList logNodes = node.getChildNodes();
		      for (int j = 0; j < logNodes.getLength(); j++) {
		    	  Element element = (Element) logNodes.item(j);
    		      NamedNodeMap nodeMap = element.getAttributes() ;
    		      code = nodeMap.getNamedItem("Title").getNodeValue();
    		      message =  nodeMap.getNamedItem("Value").getNodeValue();
    		      System.out.println("CODE:"+code+" Msg:"+ message );
		      }
		    }
		  */
		 //TODO: registrazione degli ID nel DB
		 responseValue.setReturnCodeString("OK");
		 return responseValue;
	}
	
	private EsitoResponseMPX parserReturnMessageMPXQuery(org.w3c.dom.Document  doc) throws Exception{
		EsitoResponseMPX responseValue = new EsitoResponseMPX();
		responseValue = ParserResponse.parserReturnMessage(doc,"Header","GlobalCode");
		if(responseValue.getReturnCodeInt()!=0) {
			return responseValue;
		}
		
		responseValue = ParserResponse.parserReturnMessage(doc,"Queries","Code");
		if(responseValue.getReturnCodeInt()!=0)return responseValue;
		ArrayList<ItemQuery> lista = null;
		if(ParserResponse.containTag(doc, "GetRegLetterNote")) {
			responseValue = ParserResponse.parserReturnMessage(doc,"GetRegLetterNote","Code");
			if(responseValue.getReturnCodeInt()!=0)return responseValue;
			  lista = ParserResponse.getListParserMessage(doc, "Envelope", Envelope.class);
			
		}else  if(ParserResponse.containTag(doc, "QueryCheckStatus")){
			responseValue = ParserResponse.parserReturnMessage(doc,"QueryCheckStatus","Code");
			if(responseValue.getReturnCodeInt()!=0)return responseValue;
			lista = ParserResponse.getListParserMessage(doc, "Set", Set.class);
			
		}else {
			//Altro
			responseValue.setReturnCodeInt(-1);
			responseValue.setReturnCodeString("Nessun esito ritrovato");
			return responseValue;
		}
		
		for (ItemQuery item : lista ) {
			if(item==null) throw new Exception("Item Query response Null");
			if(item.getType().equals("Set")&& item.getCode().equals("0")){
				//OK
			}else if(item.getType().equals("Envelope") ){
				//OK
			}else {
				responseValue.setReturnCodeInt(-1);
				responseValue.setReturnCodeString("Errore presente nel "+item.getType()+" ");
				return responseValue;
			}
		}
		//Se è tutto andato bene, metto il risultato della lista in output così da gestirla successivamente per lo store
		responseValue.setListaItemQuery(lista);
		 /*
		 NodeList Logs = doc.getElementsByTagName("Logs");
		 for (int i = 0; i < Logs.getLength(); i++) {
		      Node node =  Logs.item(i);
		      NodeList logNodes = node.getChildNodes();
		      for (int j = 0; j < logNodes.getLength(); j++) {
		    	  Element element = (Element) logNodes.item(j);
    		      NamedNodeMap nodeMap = element.getAttributes() ;
    		      code = nodeMap.getNamedItem("Title").getNodeValue();
    		      message =  nodeMap.getNamedItem("Value").getNodeValue();
    		      System.out.println("CODE:"+code+" Msg:"+ message );
		      }
		    }
		  */
		 //TODO: registrazione degli ID nel DB
		 responseValue.setReturnCodeString("OK");
		 return responseValue;
	}
	
	private org.w3c.dom.Document    callURL(String  stringURL ,String XMLSRequest, HttpURLConnection conn  ) throws IOException, ParserConfigurationException, SAXException {
		 URL  url = new URL(stringURL);
		 if(url.getProtocol().equalsIgnoreCase("https")) {
			 conn = (HttpsURLConnection) url.openConnection();
		 }else {
			 conn = (HttpURLConnection) url.openConnection();
		 }
		 conn.setRequestMethod("POST");
 		
		//conn.setRequestProperty("Content-Type", "application/json");
 		//String input = "{\"qty\":100,\"name\":\"iPad 4\"}";
		conn.setDoOutput(true);
		conn.setDoInput(true);
		//OutputStream os = conn.getOutputStream();
		//os.write(input.getBytes());
		//os.flush();
		DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
		wr.writeBytes(XMLSRequest);
		wr.flush();
		wr.close();
		//CertificateUtility.print_https_cert(conn);

		if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
			throw new RuntimeException("Failed : HTTP error code : "
				+ conn.getResponseCode() + "  " +conn.getResponseMessage());
		}
		//System.out.println("\n Esito : "+conn.getResponseCode() + "  " +conn.getResponseMessage());
		DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		InputSource is = new InputSource();
		 //is.setCharacterStream(new StringReader(message));
		is.setByteStream(conn.getInputStream());
		 //System.out.println(OperationMPXOperations.convertXmlToString(doc));
		return db.parse(is);
	}
	
	//url "https://upload.printel.com/mpx/xml/upload.asp"
	public   EsitoResponseMPX Send(  String  stringURL ,String XMLSRequest ,String type)  {
		log.info("Send MPX to "+ stringURL);
		EsitoResponseMPX responseValue = new EsitoResponseMPX();
		HttpURLConnection conn =null;
	  try {
		  Document doc = callURL(stringURL, XMLSRequest, conn);
		  System.out.println(OperationMPXOperations.convertXmlToString(doc));
		if(type.equals("UPLOAD"))
		  responseValue = parserReturnMessageMPX(doc);
		else if(type.contains("QUERY"))
			responseValue = parserReturnMessageMPXQuery(doc);
		else {
			throw new Exception("Indirizzo di invio non riconosciuto:"+stringURL);
		}
	  } catch (MalformedURLException e) {
		  responseValue.setReturnCodeString(e.getMessage());
		  responseValue.setReturnCodeInt(-2);
		e.printStackTrace();

	  } catch (IOException e) {
		  responseValue.setReturnCodeString(e.getMessage());
		  responseValue.setReturnCodeInt(-3);
		e.printStackTrace();
	 } catch (ParserConfigurationException e) {
		 responseValue.setReturnCodeString(e.getMessage());
		  responseValue.setReturnCodeInt(-4);
		  e.printStackTrace();
		 
	} catch (SAXException e) {
		responseValue.setReturnCodeString(e.getMessage());
		  responseValue.setReturnCodeInt(-5);
		e.printStackTrace();
	} catch (Exception e) {
		responseValue.setReturnCodeString(e.getMessage());
		  responseValue.setReturnCodeInt(-6);
		e.printStackTrace();
	}finally {
		 if(conn!=null) {
			 conn.disconnect();
		 }
	 }
	  //System.out.println(responseValue);
	  return responseValue;

	}
	
	

}
