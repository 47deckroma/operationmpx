package it.adobe.deck47.hdi;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import it.adobe.deck47.beans.EsitoResponseMPX;
import it.adobe.deck47.hdi.OperationMPX.OperationMPXOperations;
import it.adobe.deck47.hdi.OperationMPX.beanInstance.EsitoMPX_Utility;
import it.adobe.deck47.hdi.OperationMPX.beanInstance.MPXazioni;

/**
 *
 */
public class App 
{
    	// http://localhost:8080/RESTfulExample/json/product/post
    	public static void main(String[] args) throws Exception {
    		 OperationMPXOperations opOper = new OperationMPXOperations();
    		String xmlInput = App.readFile( "assets\\XMLTest\\BTC006_20181107_000001.xml");
    		// String xmlInput = App.readFile( "assets\\XMLTest\\testMarco.xml");
    		 String xmlBookmarks = App.readFile( "assets\\XMLTest\\bookmarkTest.xml");
    		 //String xmlInput = App.readFile( "assets\\XMLTest\\testQuietanzaErrore.xml");
    		 //	DBOperationsHDI db = new DBOperationsHDI();
    		 //db.storeLotto(xmlInput);
    		 EsitoResponseMPX response =  opOper.createMPX(xmlInput, "6", "sada", xmlBookmarks, MPXazioni.POSTALIZZAZIONE);
    		     EsitoResponseMPX responseEsito = opOper.sendToPostel(response.getMPXString(),"http://postpdx.postel.it/Upload.ashx",true,"UPLOAD");
    		System.out.println("\nESITO:" + EsitoMPX_Utility.toString(responseEsito) + responseEsito.getMPXString());
    	}
    	
    	public static String getTagValue(String xml, String tagName){
    	    return xml.split("<"+tagName+">")[1].split("</"+tagName+">")[0];
    	}
    	
    			
    		public static String readFile(String filePath) throws FileNotFoundException, IOException {
    			 String everything = null;
    			 try(BufferedReader br = new BufferedReader(new FileReader(filePath))) {
    				    StringBuilder sb = new StringBuilder("");
    				    String line = br.readLine();

    				    while (line != null) {
    				        sb.append(line);
    				        //sb.append(System.lineSeparator());
    				        line = br.readLine();
    				    }
    				    br.close();
    				     everything = sb.toString();
    				}catch(Exception e) {
    					e.printStackTrace();
    				}finally {
    					
    				}
    			 return everything;
    		 }
}
