//
// Questo file � stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andr� persa durante la ricompilazione dello schema di origine. 
// Generato il: 2018.12.13 alle 12:50:45 PM CET 
//


package it.mpx.queries;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/**
 * <p>Classe Java per anonymous complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Header">
 *           &lt;complexType>
 *             &lt;simpleContent>
 *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
 *                 &lt;attribute name="ZCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                 &lt;attribute name="Username" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                 &lt;attribute name="Password" type="{http://www.w3.org/2001/XMLSchema}string" />
 *               &lt;/extension>
 *             &lt;/simpleContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Queries">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="GetRegLetterNote" maxOccurs="10" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;simpleContent>
 *                         &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
 *                           &lt;attribute name="CustomerSetID" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="MPXSetID" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                         &lt;/extension>
 *                       &lt;/simpleContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="QueryCheckStatus">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="Set" maxOccurs="100" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;simpleContent>
 *                                   &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
 *                                     &lt;attribute name="CustomerSetID" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                     &lt;attribute name="MPXSetID" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                   &lt;/extension>
 *                                 &lt;/simpleContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "header",
    "queries"
})
@XmlRootElement(name = "MPXQuery")
public class MPXQuery {

    @XmlElement(name = "Header", required = true)
    protected MPXQuery.Header header;
    @XmlElement(name = "Queries", required = true)
    protected MPXQuery.Queries queries;

    /**
     * Recupera il valore della propriet� header.
     * 
     * @return
     *     possible object is
     *     {@link MPXQuery.Header }
     *     
     */
    public MPXQuery.Header getHeader() {
        return header;
    }

    /**
     * Imposta il valore della propriet� header.
     * 
     * @param value
     *     allowed object is
     *     {@link MPXQuery.Header }
     *     
     */
    public void setHeader(MPXQuery.Header value) {
        this.header = value;
    }

    /**
     * Recupera il valore della propriet� queries.
     * 
     * @return
     *     possible object is
     *     {@link MPXQuery.Queries }
     *     
     */
    public MPXQuery.Queries getQueries() {
        return queries;
    }

    /**
     * Imposta il valore della propriet� queries.
     * 
     * @param value
     *     allowed object is
     *     {@link MPXQuery.Queries }
     *     
     */
    public void setQueries(MPXQuery.Queries value) {
        this.queries = value;
    }


    /**
     * <p>Classe Java per anonymous complex type.
     * 
     * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;simpleContent>
     *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
     *       &lt;attribute name="ZCode" type="{http://www.w3.org/2001/XMLSchema}string" />
     *       &lt;attribute name="Username" type="{http://www.w3.org/2001/XMLSchema}string" />
     *       &lt;attribute name="Password" type="{http://www.w3.org/2001/XMLSchema}string" />
     *     &lt;/extension>
     *   &lt;/simpleContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "value"
    })
    public static class Header {

        @XmlValue
        protected String value;
        @XmlAttribute(name = "ZCode")
        protected String zCode;
        @XmlAttribute(name = "Username")
        protected String username;
        @XmlAttribute(name = "Password")
        protected String password;
        
        
        public Header() {
        	super();
        }

        public Header(String zCode, String username, String password) {
			super();
			this.zCode = zCode;
			this.username = username;
			this.password = password;
		}

		/**
         * Recupera il valore della propriet� value.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getValue() {
            return value;
        }

        /**
         * Imposta il valore della propriet� value.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setValue(String value) {
            this.value = value;
        }

        /**
         * Recupera il valore della propriet� zCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getZCode() {
            return zCode;
        }

        /**
         * Imposta il valore della propriet� zCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setZCode(String value) {
            this.zCode = value;
        }

        /**
         * Recupera il valore della propriet� username.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUsername() {
            return username;
        }

        /**
         * Imposta il valore della propriet� username.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUsername(String value) {
            this.username = value;
        }

        /**
         * Recupera il valore della propriet� password.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPassword() {
            return password;
        }

        /**
         * Imposta il valore della propriet� password.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPassword(String value) {
            this.password = value;
        }

    }


    /**
     * <p>Classe Java per anonymous complex type.
     * 
     * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="GetRegLetterNote" maxOccurs="10" minOccurs="0">
     *           &lt;complexType>
     *             &lt;simpleContent>
     *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
     *                 &lt;attribute name="CustomerSetID" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="MPXSetID" type="{http://www.w3.org/2001/XMLSchema}string" />
     *               &lt;/extension>
     *             &lt;/simpleContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="QueryCheckStatus">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="Set" maxOccurs="100" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;simpleContent>
     *                         &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
     *                           &lt;attribute name="CustomerSetID" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                           &lt;attribute name="MPXSetID" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                         &lt;/extension>
     *                       &lt;/simpleContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "getRegLetterNote",
        "queryCheckStatus"
    })
    public static class Queries {

        @XmlElement(name = "GetRegLetterNote")
        protected List<MPXQuery.Queries.GetRegLetterNote> getRegLetterNote;
        @XmlElement(name = "QueryCheckStatus", required = true)
        protected MPXQuery.Queries.QueryCheckStatus queryCheckStatus;

        /**
         * Gets the value of the getRegLetterNote property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the getRegLetterNote property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getGetRegLetterNote().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link MPXQuery.Queries.GetRegLetterNote }
         * 
         * 
         */
        public List<MPXQuery.Queries.GetRegLetterNote> getGetRegLetterNote() {
            if (getRegLetterNote == null) {
                getRegLetterNote = new ArrayList<MPXQuery.Queries.GetRegLetterNote>();
            }
            return this.getRegLetterNote;
        }

        /**
         * Recupera il valore della propriet� queryCheckStatus.
         * 
         * @return
         *     possible object is
         *     {@link MPXQuery.Queries.QueryCheckStatus }
         *     
         */
        public MPXQuery.Queries.QueryCheckStatus getQueryCheckStatus() {
            return queryCheckStatus;
        }

        /**
         * Imposta il valore della propriet� queryCheckStatus.
         * 
         * @param value
         *     allowed object is
         *     {@link MPXQuery.Queries.QueryCheckStatus }
         *     
         */
        public void setQueryCheckStatus(MPXQuery.Queries.QueryCheckStatus value) {
            this.queryCheckStatus = value;
        }


        /**
         * <p>Classe Java per anonymous complex type.
         * 
         * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;simpleContent>
         *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
         *       &lt;attribute name="CustomerSetID" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="MPXSetID" type="{http://www.w3.org/2001/XMLSchema}string" />
         *     &lt;/extension>
         *   &lt;/simpleContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "value"
        })
        public static class GetRegLetterNote {

            @XmlValue
            protected String value;
            @XmlAttribute(name = "CustomerSetID")
            protected String customerSetID;
            @XmlAttribute(name = "MPXSetID")
            protected String mpxSetID;

            /**
             * Recupera il valore della propriet� value.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getValue() {
                return value;
            }

            /**
             * Imposta il valore della propriet� value.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setValue(String value) {
                this.value = value;
            }

            /**
             * Recupera il valore della propriet� customerSetID.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCustomerSetID() {
                return customerSetID;
            }

            /**
             * Imposta il valore della propriet� customerSetID.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCustomerSetID(String value) {
                this.customerSetID = value;
            }

            /**
             * Recupera il valore della propriet� mpxSetID.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getMPXSetID() {
                return mpxSetID;
            }

            /**
             * Imposta il valore della propriet� mpxSetID.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setMPXSetID(String value) {
                this.mpxSetID = value;
            }

        }


        /**
         * <p>Classe Java per anonymous complex type.
         * 
         * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="Set" maxOccurs="100" minOccurs="0">
         *           &lt;complexType>
         *             &lt;simpleContent>
         *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
         *                 &lt;attribute name="CustomerSetID" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                 &lt;attribute name="MPXSetID" type="{http://www.w3.org/2001/XMLSchema}string" />
         *               &lt;/extension>
         *             &lt;/simpleContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "set"
        })
        public static class QueryCheckStatus {

            @XmlElement(name = "Set")
            protected List<MPXQuery.Queries.QueryCheckStatus.Set> set;

            /**
             * Gets the value of the set property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the set property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getSet().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link MPXQuery.Queries.QueryCheckStatus.Set }
             * 
             * 
             */
            public List<MPXQuery.Queries.QueryCheckStatus.Set> getSet() {
                if (set == null) {
                    set = new ArrayList<MPXQuery.Queries.QueryCheckStatus.Set>();
                }
                return this.set;
            }


            /**
             * <p>Classe Java per anonymous complex type.
             * 
             * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;simpleContent>
             *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
             *       &lt;attribute name="CustomerSetID" type="{http://www.w3.org/2001/XMLSchema}string" />
             *       &lt;attribute name="MPXSetID" type="{http://www.w3.org/2001/XMLSchema}string" />
             *     &lt;/extension>
             *   &lt;/simpleContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "value"
            })
            public static class Set {

                @XmlValue
                protected String value;
                @XmlAttribute(name = "CustomerSetID")
                protected String customerSetID;
                @XmlAttribute(name = "MPXSetID")
                protected String mpxSetID;

                /**
                 * Recupera il valore della propriet� value.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getValue() {
                    return value;
                }

                /**
                 * Imposta il valore della propriet� value.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setValue(String value) {
                    this.value = value;
                }

                /**
                 * Recupera il valore della propriet� customerSetID.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCustomerSetID() {
                    return customerSetID;
                }

                /**
                 * Imposta il valore della propriet� customerSetID.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCustomerSetID(String value) {
                    this.customerSetID = value;
                }

                /**
                 * Recupera il valore della propriet� mpxSetID.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getMPXSetID() {
                    return mpxSetID;
                }

                /**
                 * Imposta il valore della propriet� mpxSetID.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setMPXSetID(String value) {
                    this.mpxSetID = value;
                }

            }

        }

    }

}
